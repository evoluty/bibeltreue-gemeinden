"""
Definition of urls for bibeltreue_gemeinden.
"""

from datetime import datetime
from django.conf.urls import url, include
from django.conf.urls.i18n import i18n_patterns
import django.contrib.auth.views as auth_views
from django.contrib.staticfiles.storage import staticfiles_storage
from django.views.generic.base import RedirectView

import app.views

# Uncomment the next lines to enable the admin:
from django.contrib import admin

admin.autodiscover()

# Uncomment the next line to enable the admin:
urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(
        r'^favicon.ico$',
        RedirectView.as_view(
            url=staticfiles_storage.url('favicon.ico'),
            permanent=False),
        name="favicon"
    ),
]
urlpatterns += i18n_patterns(
    url(r'^$', app.views.home, name='home'),
    url(r'^contact$', app.views.contact, name='contact'),
    url(r'^about', app.views.about, name='about'),
    url(r'^impressum', app.views.impressum, name='impressum'),
    url(r'^datenschutz', app.views.datenschutz, name='datenschutz'),
    url(r'^accounts/login/$',
        auth_views.login,
        {
            'template_name': 'app/login.html',
            'authentication_form': app.forms.BootstrapAuthenticationForm,
            'extra_context':
                {
                    'title': 'Log in',
                    'year': datetime.now().year,
                }
        },
        name='login'),
    url(r'^accounts/logout$',
        auth_views.logout,
        {
            'next_page': '/',
        },
        name='logout'),
    url(r'^accounts/signup/$', app.views.signup, name='signup'),
    url(r'^ajax/osm_location/$', app.views.get_location_from_osm, name="osm_location"),
    url(r'^ajax/bounding_box/$', app.views.get_places_by_bounding_box, name="bounding_box"),
    url(r'^ajax/all_places/$', app.views.get_all_places, name="all_places"),
    url(r'^accounts/account_activation_sent/$', app.views.account_activation_sent, name='account_activation_sent'),
    url(r'^accounts/activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        app.views.activate, name='activate'),
    url(r'^accounts/update_profile/$', app.views.update_profile, name="update_profile"),
    url(r'^accounts/password_reset/$', auth_views.password_reset,
        {
            'template_name': 'app/password_reset_form.html',
            'extra_context':
                {
                    'year': datetime.now().year,
                }
        },
        name='password_reset'),
    url(r'^accounts/password_reset/done/$', auth_views.password_reset_done, name='password_reset_done'),
    url(r'^accounts/reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm, name='password_reset_confirm'),
    url(r'^accounts/reset/done/$', auth_views.password_reset_complete, name='password_reset_complete'),
    url(r'^places/$', app.views.all_places, name="places"),
    url(r'^places/add/$', app.views.add_place, name="add_place"),
    url(r'^places/(?P<id>[0-9]+)/$', app.views.edit_place, name="edit_place"),
    url(r'^places/batch/$', app.views.batch_add_places, name="batch_add"),
    url(r'^places/command/$', app.views.submit_command, name="command"),

    # url(r'^waypoints/', include('waypoints.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
