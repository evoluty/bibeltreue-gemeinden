from django.apps import AppConfig


class waypointsConfig(AppConfig):
    name = 'waypoints'
