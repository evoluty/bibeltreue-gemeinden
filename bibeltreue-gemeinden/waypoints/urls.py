# Import django modules
from django.conf.urls import url
from waypoints import views


urlpatterns = [
    url(r'^$', views.index, name='waypoints-index'),
    url(r'^save$', views.save, name='waypoints-save'),
]
