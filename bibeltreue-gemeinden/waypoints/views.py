from django.shortcuts import render, HttpResponse
from django.template.loader import render_to_string
from waypoints.models import Waypoint


# Create your views here.
def index(request):
    waypoints = Waypoint.objects.order_by('name')
    return render(request,'index.html',{
        'waypoints': waypoints,
        'content': render_to_string('waypoints.html', {'waypoints': waypoints})})

def save(request):
    'Save waypoints'
    for waypointString in request.POST.get('waypointsPayload', '').splitlines():
        waypointID, waypointX, waypointY = waypointString.split()
        waypoint = Waypoint.objects.get(id=int(waypointID))
        waypoint.geometry.set_x(float(waypointX))
        waypoint.geometry.set_y(float(waypointY))
        waypoint.save()
    return HttpResponse(simplejson.dumps(dict(isOk=1)), mimetype='application/json')