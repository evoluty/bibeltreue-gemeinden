# !/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import csv
from django.utils.encoding import smart_str, smart_unicode

data_with_points = [
    [u'Straße,Nummer', u'PLZ', u'Stadt', u'Verantwortlicher', u'Telefon', u'Ort Zusatz', u'Filiale', u'Bezeichnung',
     u'Bruderschaft', u'Position'],
    [u'Haunstetterstraße', u'101', u'DE 86161', u'Augsburg', u'Peter Fot', u'.+498215896658', u'', u'',
     u'Evangeliums-Christen Baptisten', u'Vereinigung der Evangeliums-Christen Baptisten',
     u'POINT(10.906862 48.341578)']]

if __name__ == "__main__":
    dominations = dict()
    bruderschaften = dict()

    # with open("liste.csv", "rb") as f:
    #     reader = csv.reader(f)
    #
    #     for row in reader:
    #         domination = unicode(row[8])
    #         # domination = row[8].encode('raw_unicode_escape').decode('utf8')
    #         # bruderschaft = row[9].encode('raw_unicode_escape').decode('utf8')
    #         bruderschaft = unicode(row[9])
    #         dominations.update({domination: ""})
    #         bruderschaften.update({bruderschaft: ""})

    for c in data_with_points:
        if len(c) < 10:
            print (c)
        domination = unicode(c[8]).encode('raw_unicode_escape').decode('raw_unicode_escape')
        print (domination)
        bruderschaft = unicode(c[9]).encode('raw_unicode_escape').decode('raw_unicode_escape')
        dominations.update({domination: ""})
        bruderschaften.update({bruderschaft: ""})

    print (dominations)
    print (bruderschaften)
    # print unicode(u"Straße").encode("utf-8")
    # print unicode(u"Straße").encode('raw_unicode_escape').decode('utf8')
