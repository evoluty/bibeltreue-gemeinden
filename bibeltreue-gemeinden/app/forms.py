"""
Definition of forms.
"""

from django.contrib.gis import forms
from django.contrib.auth.models import User
from app.models import Profile, Place, Domination, Bruderschaft
from app.custom_fields import CharPointField
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.utils.translation import ugettext_lazy as _


class BootstrapAuthenticationForm(AuthenticationForm):
    """Authentication form which uses boostrap CSS."""
    username = forms.CharField(max_length=254,
                               widget=forms.TextInput({
                                   'class': 'form-control',
                                   'placeholder': _('User name')}))
    password = forms.CharField(label=_("Password"),
                               widget=forms.PasswordInput({
                                   'class': 'form-control',
                                   'placeholder': _('Password')}))


class BootstrapSignUpForm(UserCreationForm):
    username = forms.CharField(max_length=254,
                               widget=forms.TextInput({
                                   'class': 'form-control',
                                   'placeholder': _('User name')}))
    email = forms.EmailField(max_length=254, help_text=_('Required. Inform a valid email address.'),
                             widget=forms.TextInput({
                                 'class': 'form-control',
                                 'placeholder': _('E-Mail')}))

    password1 = forms.CharField(label=_("Password"),
                                widget=forms.PasswordInput({
                                    'class': 'form-control',
                                    'placeholder': _('Password')}))

    password2 = forms.CharField(label=_("Retype Password"),
                                widget=forms.PasswordInput({
                                    'class': 'form-control',
                                    'placeholder': _('Retype Password')}))

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2',)


class UserForm(forms.ModelForm):
    first_name = forms.CharField(max_length=254,
                                 widget=forms.TextInput({
                                     'class': 'form-control',
                                     'placeholder': _('First Name')}))
    last_name = forms.CharField(max_length=254,
                                widget=forms.TextInput({
                                    'class': 'form-control',
                                    'placeholder': _('Last Name')}))

    email = forms.EmailField(max_length=254,
                             widget=forms.TextInput({
                                 'class': 'form-control',
                                 'placeholder': _('E-Mail')}))

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email',)


class ProfileForm(forms.ModelForm):
    bio = forms.CharField(widget=forms.Textarea({
        'class': 'form-control',
        'title': _('Describe yourself'),
        'placeholder': _('Describe yourself')}), required=False)

    class Meta:
        model = Profile
        fields = ('bio',)


class PlaceForm(forms.ModelForm):
    name = forms.CharField(max_length=254,
                           widget=forms.TextInput({
                               'class': 'form-control',
                               'placeholder': _('Name')}))

    description = forms.CharField(widget=forms.Textarea({
        'class': 'form-control',
        'title': _('Describe this Place'),
        'placeholder': _('description')}), required=False)

    street = forms.CharField(min_length=4, max_length=254,
                             widget=forms.TextInput({
                                 'class': 'form-control',
                                 'placeholder': _('Street')}))

    hous_number = forms.CharField(max_length=254,
                                  widget=forms.TextInput({
                                      'class': 'form-control',
                                      'placeholder': _('Hous Number')}))

    postcode = forms.CharField(max_length=254,
                               widget=forms.TextInput({
                                   'class': 'form-control',
                                   'placeholder': _('Postcode')}))

    city = forms.CharField(max_length=254,
                           widget=forms.TextInput({
                               'class': 'form-control',
                               'placeholder': _('City')}))

    country = forms.CharField(max_length=254, initial='Deutschland',
                              widget=forms.TextInput({
                                  'class': 'form-control',
                                  'placeholder': _('Country')}))
    contact_name = forms.CharField(max_length=254,
                                   widget=forms.TextInput({
                                       'class': 'form-control',
                                       'placeholder': _('First and family name')}), required=False)
    contact_phone = forms.CharField(max_length=254,
                                    widget=forms.TextInput({
                                        'class': 'form-control',
                                        'placeholder': _('Phone')}), required=False)
    contact_e_mail = forms.EmailField(max_length=254,
                                      widget=forms.TextInput({
                                          'class': 'form-control',
                                          'placeholder': _('E-Mail')}), required=False)

    contact_website = forms.URLField(max_length=254,
                                     widget=forms.TextInput({
                                         'class': 'form-control',
                                         'placeholder': _('Website')}), required=False)

    bruderschaft = forms.ModelChoiceField(queryset=Bruderschaft.objects.all(), required=False)

    domination = forms.ModelChoiceField(queryset=Domination.objects.all(), required=False)

    position = CharPointField(widget=forms.TextInput({
        'class': 'form-control',
        'placeholder': _('From map'),
        'readonly': 'readonly',
        'title': _('Use map to set'), }))

    class Meta:
        model = Place
        fields = (
        'name', 'street', 'hous_number', 'postcode', 'city', 'country', 'description', 'contact_name', 'contact_phone',
        'contact_e_mail', 'contact_website', 'position', 'bruderschaft', 'domination')
