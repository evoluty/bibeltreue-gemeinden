# !/usr/bin/python
# -*- coding: utf-8 -*-
"""
Definition of views.
"""
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth import login
from django.contrib import messages
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from django.http import HttpRequest, JsonResponse
from django.contrib.gis.geos import GEOSGeometry, Polygon
from django.contrib.gis.measure import Distance
from datetime import datetime
from django.core.mail import send_mail
from django.utils.translation import ugettext_lazy as _

import requests
import json

from app.forms import BootstrapSignUpForm, UserForm, ProfileForm, PlaceForm
from app.tokens import account_activation_token
from app.models import *
import initial_import


def get_base_template_data():
    template_data = {'year': datetime.now().year}
    return template_data


def home(request):
    """Renders the home page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/index.html',
        {
            'title': _('Home'),
            'year': datetime.now().year,
        }
    )


def contact(request):
    """Renders the contact page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/contact.html',
        {
            'title': _('Contact'),
            'message': _('Your contact page.'),
            'year': datetime.now().year,
        }
    )


def about(request):
    """Renders the about page."""
    assert isinstance(request, HttpRequest)
    template_data = get_base_template_data()
    template_data.update({'title': _('About')})
    return render(request, 'app/about.html', template_data)


def impressum(request):
    """Renders the about page."""
    assert isinstance(request, HttpRequest)
    template_data = get_base_template_data()
    template_data.update({'title': _('Impressum')})
    return render(request, 'app/impressum.html', template_data)

def datenschutz(request):
    """Renders the about page."""
    assert isinstance(request, HttpRequest)
    template_data = get_base_template_data()
    template_data.update({'title': _('Datenschutz')})
    return render(request, 'app/datenschutz.html', template_data)


def signup(request):
    if request.method == 'POST':
        form = BootstrapSignUpForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()
            current_site = get_current_site(request)
            subject = _("Activate your Account on Bibeltreue Gemeinden")
            message = render_to_string('app/account_activation_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': account_activation_token.make_token(user),
            })
            user.email_user(subject, message)
            return account_activation_sent(request)
    else:
        form = BootstrapSignUpForm()
    return render(request, 'app/signup.html', {'form': form, 'title': _('Register'), 'year': datetime.now().year, })


def account_activation_sent(request):
    return render(request, 'app/account_activation_sent.html')


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.profile.email_confirmed = True
        user.save()
        login(request, user)
        return redirect('home')
    else:
        return render(request, 'app/account_activation_invalid.html')


@login_required
@transaction.atomic
def update_profile(request):
    if request.method == 'POST':
        user_form = UserForm(request.POST, instance=request.user)
        profile_form = ProfileForm(request.POST, instance=request.user.profile)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request, _('Your profile was successfully updated!'))
            # return redirect('/update_profile')

        else:
            messages.error(request, _('Please correct the error below.'))
    else:
        user_form = UserForm(instance=request.user)
        profile_form = ProfileForm(instance=request.user.profile)
    return render(request, 'app/profile.html', {
        'user_form': user_form,
        'profile_form': profile_form,
        'title': _('Update Profile'), 'year': datetime.now().year,
    })


def all_places(request):
    template_data = get_base_template_data()
    places = Place.objects.all()
    template_data.update({'places': places, 'title': _('All Places')})

    return render(request, 'app/places.html', template_data)


@login_required()
def add_place(request):
    template_data = get_base_template_data()

    if request.method == 'POST':
        place_form = PlaceForm(request.POST)
        if place_form.is_valid():
            my_place = place_form.save()
            messages.success(request, _('Place has been successfully created.'))
            return redirect('edit_place', id=my_place.id)
            # template_data.update({'place_form': place_form, 'title': 'Edit Place'})

        else:
            messages.error(request, _('Please correct the error below.'))
    else:
        place_form = PlaceForm()
        template_data.update({'place_form': place_form, 'title': _('Add Place')})

    return render(request, 'app/add_place.html', template_data)


@login_required()
def edit_place(request, id):
    template_data = get_base_template_data()

    my_place = Place.objects.get(id=id)

    if request.method == 'POST':
        place_form = PlaceForm(request.POST, instance=my_place)
        if place_form.is_valid():
            place_form.save()
            # place_form = PlaceForm(instance=my_place)
            messages.success(request, _('Place has been successfully updated.'))
            template_data.update({'place_form': place_form, 'title': _('Edit Place')})

        else:
            messages.error(request, _('Please correct the error below.'))
    else:
        place_form = PlaceForm(instance=my_place)
        template_data.update({'place_form': place_form, 'title': _('Add Place')})

    return render(request, 'app/edit_place.html', template_data)


def get_days_diff(place):
    if place.added:
        now = datetime.utcnow().replace(tzinfo=None)
        timediff = now - place.added.replace(tzinfo=None)
        return timediff.days


def get_location_from_internet(search_string):
    url = "https://unwiredlabs.com/v2/search.php"

    oms_request_data = {
        'token': '94b10d4791a40b',
        'q': search_string
    }
    response = requests.get(url, params=oms_request_data)
    json_data = json.loads(response.text)

    return json_data


def render_marker(place):
    passed_days = get_days_diff(place)

    html = render_to_string('app/marker_popup.html', {'place': place})
    marker = {'location': {'lon': place.position.x, 'lat': place.position.y}, 'passed_days': passed_days,
              'popup': html}
    return marker


def get_location_from_osm(request):
    data = {"query": ""}
    if request.method == 'GET':
        data = request.GET

    search_string = data.get('query', '')

    json_data = get_location_from_internet(search_string)

    return_dict = {'status': 'error'}

    if json_data['status'] == 'ok':
        return_dict['status'] = 'ok'
        first_address = json_data['address'][0]
        center = {'lat': first_address['lat'], 'lon': first_address['lon']}
        return_dict['center'] = center

        point = 'POINT({lon} {lat})'.format(lat=center['lat'], lon=center['lon'])
        geom = GEOSGeometry(point, srid=4326)
        radius = int(data.get('radius', 10))
        distance = Distance(km=radius)
        places = Place.objects.filter(position__distance_lte=(geom, distance))
        markers = []
        for place in places:
            marker = render_marker(place)
            markers.append(marker)

        return_dict['markers'] = markers

    return JsonResponse(return_dict)


def get_places_by_bounding_box(request):
    data = {"bounding_box": [[38.05664062500001, 56.022948079627454], [38.05664062500001, 46.10370875598026],
                             [-17.182617187500004, 46.10370875598026], [-17.182617187500004, 56.022948079627454]]}

    if request.method == 'GET':
        data = request.GET.dict()

    bounding_box_string = data.get('bounding_box', [])
    bounding_box_list = json.loads(bounding_box_string)
    bounding_box_list.append(bounding_box_list[0])

    cordinates = list()
    cordinates.append(bounding_box_list)

    geojson = {"type": "Polygon", "coordinates": cordinates}
    geojsonstring = json.dumps(geojson)

    geom = GEOSGeometry(geojsonstring, srid=4326)

    places = Place.objects.filter(position__intersects=geom)

    markers = []
    return_dict = {'status': 'ok'}
    for place in places:
        marker = render_marker(place)
        markers.append(marker)

    return_dict['markers'] = markers

    return JsonResponse(return_dict)


def get_all_places(request):
    places = Place.objects.all()

    markers = []
    return_dict = {'status': 'ok'}
    for place in places:
        marker = render_marker(place)
        markers.append(marker)

    return_dict['markers'] = markers

    return JsonResponse(return_dict)


@login_required()
def batch_add_places(request):
    data = initial_import.data_with_points

    first_line = True
    for church in data:
        if first_line:
            first_line = False
            continue

        street = church[0]
        house_number = church[1]
        plz = church[2]
        city = church[3]
        country = "Germany"
        contact_name = church[4]
        contact_phone = church[5]
        filiale = church[7]
        domination = church[8]
        bruderschaft = church[9]
        point = church[10]

        name = domination
        description = ""

        if plz.startswith("BE"):
            country = "Belgium"
        elif plz.startswith("CZ"):
            country = "Czech Republic"
        elif plz.startswith("IL"):
            country = "Israel"

        contact_phone = contact_phone[1:]
        geom = GEOSGeometry(point, srid=4326)

        db_domination, created = Domination.objects.get_or_create(name=domination, description="")
        db_bruderschaft, created = Bruderschaft.objects.get_or_create(name=bruderschaft, description="")

        try:
            if filiale:
                description = u"Filiale der Gemeinde {0}".format(filiale)

            if church[6]:
                name = name + u" " + church[6]
            else:
                name = name + u" " + city
        except UnicodeEncodeError:
            pass

        place, created = Place.objects.get_or_create(name=name, description=description, street=street,
                                                     hous_number=house_number,
                                                     postcode=plz, city=city, country=country,
                                                     contact_name=contact_name, contact_phone=contact_phone,
                                                     position=geom)
        place.bruderschaft = db_bruderschaft
        place.domination = db_domination
        place.save()

    return all_places(request)


def submit_command(request):

    send_mail(
        'Subject here',
        'Here is the message.',
        'info@bibeltreue-gemeinden.de',
        ['anux.linux@gmail.com'],
        fail_silently=False,
    )


    #places = Place.objects.all()

    #for place in places:
        #latitude = place.position.x
        #longitude = place.position.y
        # place.position.x = longitude
        # place.position.y = latitude
        #print place.position.x
        #print place.position.y
        # place.save()

    return home(request)
