from django.contrib.gis import admin
from app.models import *

# Register your models here.

admin.site.register(Profile, admin.GeoModelAdmin)
admin.site.register(Bruderschaft, admin.GeoModelAdmin)
admin.site.register(Domination, admin.GeoModelAdmin)
admin.site.register(Contact, admin.GeoModelAdmin)
admin.site.register(Place, admin.GeoModelAdmin)
admin.site.register(ReccuringEvent, admin.GeoModelAdmin)
admin.site.register(SingleEvent, admin.GeoModelAdmin)
