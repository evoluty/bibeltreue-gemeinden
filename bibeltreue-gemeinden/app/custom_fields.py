import re
from django.utils.translation import ugettext as _
from django.contrib.gis.db import models
from django.contrib.gis import forms
from django.contrib.gis.geos import Point
from django.core.exceptions import ValidationError
from django.utils.encoding import force_text

DAY_OF_THE_WEEK = {
    '1': _(u'Monday'),
    '2': _(u'Tuesday'),
    '3': _(u'Wednesday'),
    '4': _(u'Thursday'),
    '5': _(u'Friday'),
    '6': _(u'Saturday'),
    '7': _(u'Sunday'),
}


class DayOfTheWeekField(models.CharField):
    def __init__(self, *args, **kwargs):
        kwargs['choices'] = tuple(sorted(DAY_OF_THE_WEEK.items()))
        kwargs['max_length'] = 1
        super(DayOfTheWeekField, self).__init__(*args, **kwargs)


class CharPointField(forms.Field):
    default_error_messages = {
        'invalid': _('Enter latitude,longitude'),
    }
    re_point = re.compile(r'^\s*(-?\d+(?:\.\d+)?),\s*(-?\d+(?:\.\d+)?)\s*$')

    def prepare_value(self, value):
        if isinstance(value, Point):
            return "{},{}".format(*value.coords)
        return value

    def to_python(self, value):
        """
        Validates input. Returns a Point instance or None for empty values.
        """
        value = super(CharPointField, self).to_python(value)
        if value in self.empty_values:
            return None
        try:
            m = self.re_point.match(force_text(value))
            if not m:
                raise ValueError()
            value = Point(float(m.group(1)), float(m.group(2)))
        except (ValueError, TypeError):
            raise ValidationError(self.error_messages['invalid'],
                                  code='invalid')
        return value
