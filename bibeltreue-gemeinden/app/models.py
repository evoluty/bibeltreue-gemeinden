"""
Definition of models.
"""

from django.contrib.gis.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from app import custom_fields


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bio = models.TextField(max_length=500, blank=True)
    email_confirmed = models.BooleanField(default=False)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()


@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    instance.profile.save()


class Bruderschaft(models.Model):
    name = models.CharField(max_length=250, blank=False, unique=True)
    description = models.TextField()

    def __unicode__(self):
        return u'{obj.name}'.format(obj=self)


class Domination(models.Model):
    name = models.CharField(max_length=250, blank=False, unique=True)
    description = models.TextField()

    def __unicode__(self):
        return u'{obj.name}'.format(obj=self)


class Contact(models.Model):
    first_name = models.CharField(max_length=50, blank=False)
    last_name = models.CharField(max_length=50, blank=False)
    # phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone
    # number must be entered in the format: '+999999999'.  Up to 15 digits
    # allowed.")
    phone_number = models.CharField(max_length=15, blank=False)
    e_mail = models.EmailField(max_length=256, blank=True)
    website = models.URLField(max_length=500, blank=True)

    def __unicode__(self):
        return u'{obj.first_name} {obj.last_name} {obj.phone_number} {obj.e_mail} {obj.website}'.format(obj=self)


class Place(models.Model):
    name = models.CharField(max_length=250, blank=False)
    description = models.TextField(blank=True)

    street = models.CharField(max_length=120, blank=False)
    hous_number = models.CharField(max_length=10, blank=False)
    postcode = models.CharField(max_length=10, blank=False)
    city = models.CharField(max_length=250, blank=False)
    country = models.CharField(max_length=250, blank=False)

    bruderschaft = models.ForeignKey(Bruderschaft, blank=True, null=True)
    domination = models.ForeignKey(Domination, blank=True, null=True)
    contact_name = models.CharField(max_length=250, blank=True)
    contact_phone = models.CharField(max_length=15, blank=False)
    contact_e_mail = models.EmailField(max_length=256, blank=True)
    contact_website = models.URLField(max_length=500, blank=True)

    position = models.PointField(srid=4326)
    objects = models.GeoManager

    added = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return u'{obj.name}: {obj.street} {obj.hous_number} {obj.postcode} {obj.city} ({obj.position.x}, {obj.position.y})'.format(
            obj=self)


class ReccuringEvent(models.Model):
    name = models.CharField(max_length=250, blank=False)
    description = models.TextField(blank=False)

    day_of_week = custom_fields.DayOfTheWeekField(blank=False)
    time = models.TimeField(blank=False)
    duration = models.DurationField(blank=True)

    place = models.ForeignKey(Place, on_delete=models.CASCADE)

    def __unicode__(self):
        return u'{obj.name} {obj.description}'.format(obj=self)


class SingleEvent(models.Model):
    name = models.CharField(max_length=250, blank=False)
    description = models.TextField(blank=False)

    date = models.DateField(blank=False)
    time = models.TimeField(blank=False)
    duration = models.DurationField(blank=True)

    place = models.ForeignKey(Place, on_delete=models.CASCADE)

    def __unicode__(self):
        return u'{obj.name} {obj.description}'.format(obj=self)
